const express = require("express");
const cors = require("cors");
const bookingRoute = require("./routes/bookingRoutes");
const newsLetterRoute = require('./routes/newsLetterRoutes');
const newsRoute = require('./routes/newsRoutes');
const eventRoute = require('./routes/eventRoutes');
const adminRoute = require('./routes/adminRoutes');

const app = express();

app.use(cors());

// Increase the payload size limit
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));

app.use('/api/v1/booking', bookingRoute);
app.use('/api/v1/news', newsLetterRoute);
app.use('/api/v1/currentNews', newsRoute);
app.use('/api/v1/events',eventRoute);
app.use('/api/v1/admin',adminRoute);

module.exports = app;
