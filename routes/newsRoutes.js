const express = require('express');
const router = express.Router();
const newsController = require('../controllers/newsController');

router.post('/add',newsController.createNews);
router.get('/getAllNews',newsController.getAllNews);
router.delete('/deleteNews/:id', newsController.deleteNews);
router.post('/getOneFacility',newsController.getOneFacility);
router.post("/getMorePhotos",newsController.getMorePhotos);

module.exports = router;