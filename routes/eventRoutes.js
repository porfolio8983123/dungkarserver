const express = require('express');
const router = express.Router();
const eventController = require('../controllers/eventsController');

router.post('/add',eventController.createEvent);
router.post("/get",eventController.getAllEvents);
router.get('/getEvents',eventController.getEvents);
router.delete('/events/:id', eventController.deleteEvent);

module.exports = router;