const express = require("express");
const router = express.Router();
const bookingController = require("../controllers/bookingController");

router.post('/bookConference',bookingController.bookConference);
router.get('/allBooked',bookingController.getAllBookings);
router.patch('/updateStatus/:id', bookingController.updateStatus);
router.patch('/updateApprovalCompleted/:id',bookingController.updateAprovalCompleted);
router.patch('/updateCancelled/:id',bookingController.updateCancelled);

module.exports = router;