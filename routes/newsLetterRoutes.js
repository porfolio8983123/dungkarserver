const express = require("express");
const router = express.Router();
const newsLetterController = require("../controllers/newsLetterController");

router.post("/addNewsLetter",newsLetterController.createNewsLetter);
router.get("/getAllEmail",newsLetterController.getAllListedEmail);

module.exports = router;