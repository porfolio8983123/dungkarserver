const express = require('express');
const router = express.Router();
const adminController = require("../controllers/adminController");

router.get("/getAllAdmin",adminController.getAdmin);
router.post("/login",adminController.login);
router.post('/changePassword',adminController.changePassword);

module.exports = router;