const app = require("./app");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config();

const DB = process.env.DATABASE.replace(
    'PASSWORD', 
    process.env.DATABASE_PASSWORD
)

mongoose.connect(DB)
.then((con) => {
    console.log("DB connection successfull!")
})
.catch((error) => {
    console.log(error)
})

app.listen(process.env.PORT, () => {
    console.log(`Application running on ${process.env.PORT}`)
})
