const mongoose = require("mongoose");
const validator = require("validator");

const newsLetterSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true,'Please provide your email!'],
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: 'Please provide a valid email address!'
        }
    }
})

const NewsLetter = mongoose.model("NewsLetter",newsLetterSchema);
module.exports = NewsLetter;