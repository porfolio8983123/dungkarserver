const mongoose = require("mongoose");

const conferenceSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Please provide conference name!"]
    }
})

const Conference = mongoose.model("Conference",conferenceSchema);

module.exports = Conference;