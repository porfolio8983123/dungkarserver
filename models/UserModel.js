const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    CID: {
        type: Number,
        required: [true, 'Please provide CID!']
    },
    name: {
        type: String,
        required: [true, 'Please provide user name!']
    },
    phone: {
        type: Number,
        required: [true,'Please provide phone number!']
    },
    email: {
        type: String,
        required: [true, "Please provide email!"]
    },
    organization: {
        type: String,
        required: [true,'Please provide your organization!']
    }
})

const User = mongoose.model("User",userSchema);
module.exports = User;