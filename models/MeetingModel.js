const mongoose = require("mongoose");

const meetingSchema = new mongoose.Schema({
    date: {
        type: Date,
        required: [true,"Provide current date!"]
    },
    startingTime: {
        type: String,
        required: [true, 'Please provide starting time!']
    },
    endingTime: {
        type: String,
        required: [true, 'Pleave provide ending time!']
    },
    numberOfParticipants: {
        type: Number,
        required: [true,"Please provided total participants!"]
    },
    conferenceId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Conference',
        required: [true,"Need conference Id"]
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: [true,"Need user Id"]
    },
    status: {
        type: Boolean,
        default: false
    },
    approvalCompleted: {
        type: Boolean,
        default: false
    },
    package: {
        type: Boolean,
        required: true
    },
    cancelled: {
        type: Boolean,
        default: false
    }
},{timestamps: true})

const Meeting = mongoose.model("Meeting",meetingSchema);
module.exports = Meeting;