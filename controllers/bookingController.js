const User = require('../models/UserModel');
const Conference = require("../models/ConferenceModel");
const Meeting = require("../models/MeetingModel");
const mongoose = require("mongoose");

exports.bookConference = async (req,res) => {

    const {userData,conferenceData,meetingData} = req.body;
    
    const session = await mongoose.startSession();
    session.startTransaction();

    try {

        const user = await User.create([userData],{session});

        const conference = await Conference.create([conferenceData],{session});

        const meeting = await Meeting.create([{
            ...meetingData,
            userId: user[0]._id,
            conferenceId: conference[0]._id
        }],{session})

        await session.commitTransaction();
        session.endSession();
        res.status(200).json({status: "success",message:"Facility Booked!"})
    } catch (error) {
        await session.abortTransaction();
        session.endSession();

        res.status(500).json({status:"error", message: "Booking failed", error: error.message });
    }
}

exports.getAllBookings = async (req,res) => {
    try {

        const meetings = await Meeting.find()
        .populate('userId','email name phone organization CID')
        .populate('conferenceId','name')
        .sort({ createdAt: -1 })
        .exec();

        res.status(200).json({
            status:'success',
            data: meetings
        })
        
    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Fetching bookings failed",
            error: error.message
        })
    }
}

exports.updateStatus = async (req,res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    
    try {
        const bookingId = req.params.id;
        const { status } = req.body;

        const meeting = await Meeting.findByIdAndUpdate(
            bookingId,
            { status },
            { new: true, session }
        );

        if (!meeting) {
            throw new Error("Booking not found");
        }

        await session.commitTransaction();
        session.endSession();
        
        res.status(200).json({
            status: "success",
            data: meeting,
            message: "Booking status updated successfully"
        });
    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Fetching bookings failed",
            error: error.message
        })
    }
}

exports.updateAprovalCompleted = async (req,res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    
    try {
        const bookingId = req.params.id;
        const { approvalCompleted } = req.body;

        const meeting = await Meeting.findByIdAndUpdate(
            bookingId,
            { approvalCompleted },
            { new: true, session }
        );

        if (!meeting) {
            throw new Error("Booking not found");
        }

        await session.commitTransaction();
        session.endSession();
        
        res.status(200).json({
            status: "success",
            data: meeting,
            message: "Booking updated successfully"
        });
    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Fetching bookings failed",
            error: error.message
        })
    }
}

exports.updateCancelled = async (req,res) => {
    const session = await mongoose.startSession();
    session.startTransaction();
    
    try {
        const bookingId = req.params.id;
        const { cancelled } = req.body;

        const meeting = await Meeting.findByIdAndUpdate(
            bookingId,
            { cancelled },
            { new: true, session }
        );

        if (!meeting) {
            throw new Error("Booking not found");
        }

        await session.commitTransaction();
        session.endSession();
        
        res.status(200).json({
            status: "success",
            data: meeting,
            message: "Booking updated successfully"
        });
    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Fetching bookings failed",
            error: error.message
        })
    }
}