const Event = require('../models/eventsModel');
const mongoose = require("mongoose");

exports.createEvent = async (req,res) => {

    const session = await mongoose.startSession();
    session.startTransaction();

    try {

        const event = await Event.create([req.body],{session});

        await session.commitTransaction();
        session.endSession();

        res.status(201).json({
            status: 'success',
            message: 'Event created successfully!',
            data: event,
        });
        
    } catch (error) {
        await session.abortTransaction();
        session.endSession();

        res.status(500).json({
            status: 'error',
            error: error.message
        });
    }
}

exports.getEvents = async (req,res) => {
    try {
        const allEvents = await Event.find();

        res.status(200).json({
            status: "success",
            data: allEvents.length
        });
    } catch (error) {
        res.status(500).json({
            status: 'error',
            error: error.message
        });
    }
}

exports.getAllEvents = async (req,res) => {
    try {
        const { date } = req.body;

        // Validate and parse the date
        if (!date) {
            return res.status(400).json({
                status: "error",
                message: "Date is required."
            });
        }

        // Parse the date in YYYY-MM-DD format
        const parsedDate = new Date(date);

        // Check if the parsed date is valid
        if (isNaN(parsedDate.getTime())) {
            return res.status(400).json({
                status: "error",
                message: "Invalid date format."
            });
        }

        // Ensure the provided date is only the date part without the time
        const startOfDay = new Date(parsedDate);
        startOfDay.setUTCHours(0, 0, 0, 0);

        const endOfDay = new Date(parsedDate);
        endOfDay.setUTCHours(23, 59, 59, 999);

        // Find events where the date falls within the start and end of the given day
        const allEvent = await Event.find({
            date: {
                $gte: startOfDay,
                $lt: endOfDay
            }
        }).sort({ date: -1 });

        res.status(200).json({
            status: "success",
            data: allEvent
        });

    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Fetching failed!",
            error: error.message
        });
    }
}

exports.deleteEvent = async (req, res) => {
    try {
        const { id } = req.params;

        // Validate the ID
        if (!id) {
            return res.status(400).json({
                status: "error",
                message: "Event ID is required."
            });
        }

        // Find and delete the event by ID
        const event = await Event.findByIdAndDelete(id);

        // Check if the event exists
        if (!event) {
            return res.status(404).json({
                status: "error",
                message: "Event not found."
            });
        }

        res.status(200).json({
            status: "success",
            message: "Event deleted successfully."
        });

    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Deleting event failed!",
            error: error.message
        });
    }
};