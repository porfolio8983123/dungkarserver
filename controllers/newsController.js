const News = require('../models/NewsModel');
const mongoose = require('mongoose');

exports.createNews = async (req,res) => {

    const session = await mongoose.startSession();
    session.startTransaction();

    try {
        const newNews = await News.create([req.body],{session});

        await session.commitTransaction();
        session.endSession();

        res.status(201).json({
            status: 'success',
            message: 'News created successfully!',
            data: newNews,
        });

    } catch (error) {
        await session.abortTransaction();
        session.endSession();

        res.status(500).json({
            status: 'error',
            error: error.message
        });
    }
}

exports.getAllNews = async (req,res) => {
    try {
        const allNews = await News.find().select('-morePhotos -description -updatedAt -room').sort({ createdAt: -1 });

        res.status(200).json({
            status:"success",
            data: allNews
        })
    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Fetching failed!",
            error: error.message
        })
    }
}

exports.getOneFacility = async (req,res) => {
    try {
        const { id } = req.body; // Extract the news ID from the request body
        const news = await News.findById(id).select('-morePhotos');

        res.status(200).json({
            status:"success",
            data: news
        })
    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Fetching failed!",
            error: error.message
        })
    }
}

exports.getMorePhotos = async (req,res) => {
    try {
        const { id } = req.body; // Extract the news ID from the request body
        const news = await News.findById(id).select('-title -description -coverPhoto -date -room');

        res.status(200).json({
            status:"success",
            data: news
        })
    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Fetching failed!",
            error: error.message
        })
    }
}



exports.deleteNews = async (req, res) => {
    const { id } = req.params;

    try {
        await News.findByIdAndDelete(id);
        res.status(200).json({
            status: 'success',
            message: 'News deleted successfully!',
        });
    } catch (error) {
        res.status(500).json({
            status: 'error',
            message: 'Deleting failed!',
            error: error.message,
        });
    }
};