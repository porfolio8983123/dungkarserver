const NewsLetter = require("../models/NewsLetter");
const mongoose = require("mongoose");

exports.createNewsLetter = async (req, res) => {
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
        const existingEmail = await NewsLetter.findOne({ email: req.body.email }).session(session);
        
        if (existingEmail) {
            throw new Error("You're already listed for news letter!");
        }

        await NewsLetter.create([req.body], { session });

        await session.commitTransaction();
        session.endSession();

        res.status(200).json({
            status: 'success',
            message: "Email added!"
        });
    } catch (error) {
        await session.abortTransaction();
        session.endSession();

        let errorMessage = "Failed to add newsletter";

        if (error.code === 11000) {
            errorMessage = "You're already listed for news letter!";
        } else if (error.message === "You're already listed for news letter!") {
            errorMessage = error.message;
        }

        res.status(500).json({
            status: 'error',
            message: errorMessage,
            error: error.message
        });
    }
};


exports.getAllListedEmail = async (req,res) => {
    try {
        const allEmails = await NewsLetter.find()

        res.status(200).json({
            status:"success",
            data: allEmails
        })
    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Fetching failed!",
            error: error.message
        })
    }
}