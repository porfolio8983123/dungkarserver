const Admin = require('../models/adminModel');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

exports.login = async (req,res) => {
    try {

        const {username,password} = req.body;

        if (!username || !password) {
            return res.status(401).json({
                status: 'error',
                "message":"please provide an email and password!"
            })
        }

        const admin = await Admin.findOne({username})

        if (!admin) {
            return res.status(401).json({
                status: 'error',
                message: 'Invalid username!'
            });
        }

        const isMatch = await bcrypt.compare(password,admin.password);

        if (!isMatch) {
            return res.status(401).json({
                status: 'error',
                message: 'Invalid password!'
            });
        }

        const token = jwt.sign({id:admin._id},process.env.JWT_SECRET, {
            expiresIn: process.env.JWT_EXPIRES_IN
        })

        res.status(200).json({
            status: 'success',
            data: admin,
            token
        })
    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Fetching failed!",
            error: error.message
        })
    }
}

exports.getAdmin = async (req,res) => {
    try {
        const admin = await Admin.find();

        res.status(200).json({
            status: 'success',
            data: admin
        })
    } catch (error) {
        res.status(500).json({
            status: "error",
            message: "Fetching failed!",
            error: error.message
        })
    }
}

exports.changePassword = async (req, res) => {
    try {
        const { username, oldPassword, password, confirmPassword } = req.body;

        if (!username || !oldPassword || !password || !confirmPassword) {
            return res.status(400).json({
                status: 'error',
                message: 'All fields are required!'
            });
        }

        if (password !== confirmPassword) {
            return res.status(400).json({
                status: 'error',
                message: 'New password and confirm password do not match!'
            });
        }

        const admin = await Admin.findOne({ username });

        if (!admin) {
            return res.status(404).json({
                status: 'error',
                message: 'Admin not found!'
            });
        }

        const isMatch = await bcrypt.compare(oldPassword, admin.password);

        if (!isMatch) {
            return res.status(401).json({
                status: 'error',
                message: 'Old password is incorrect!'
            });
        }

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(password, salt);

        admin.password = hashedPassword;
        await admin.save();

        res.status(200).json({
            status: 'success',
            message: 'Password changed successfully!'
        });
    } catch (error) {
        res.status(500).json({
            status: 'error',
            message: 'Password change failed!',
            error: error.message
        });
    }
};